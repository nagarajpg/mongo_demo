<?php 

class UserController extends Controller
{
	public function actionIndex()
	{	
		$mongo = new MongoClient(); // connect mongo 
		$db = $mongo->selectDB("mydb"); //select a db 
		$collection = $db->users; // select collection 
		$u = $collection->find(); //list all users
		$this->render('index',array('users' => $u));
	}

	public function actionNew()  
	{

		$this->render('create');
	}

	public function actionCreate()
	{
		 try {
		$mongo = new MongoClient();
		$db = $mongo->selectDB("mydb");
		$collection = $db->users;
		$data = $_POST;
		$user['username'] = $data['username'];
		$user['fname'] = $data['fname'];
		$user['lname'] = $data['lname'];
		$user['email'] = $data['email'];
		$user['phone'] = $data['phone'];
		$collection->insert($user);
		}
		catch (MongoException $e)
		{
		  $e->getMessage();
		}
		$u = $collection->find();
		$this->render('index',array('users'=> $u));	
	}

}