<?php

class IssueController extends Controller
{
	public function actionIndex()
	{	
		try { 
		$mongo = new MongoClient();
		$db  = $mongo->selectDB('mydb');
		$c = $db->issues;
		$iss = $c->find();
		}
		catch(MongoException $e )
		{
			$e->getMessage();
		}
		$this->render('index',array('issues'=>$iss));
	}

	public function actionRead()
	{	
		try { 
		$mongo = new MongoClient();
		$db = $mongo->selectDB('mydb');
		$c = $db->issues;
		$id = $_GET['id'];
		$iss = $c->findOne(array("_id"=> new MongoId($id)));
		}
		catch (MongoException $e)
		{
			$e->getMessage();
		}

		$this->render('read',array('issue'=>$iss));
	}

	public function actionCreateIssue()
	{
		try { 
		$mongo = new MongoClient(); // connect mongo 
		$db = $mongo->selectDB("mydb"); //select a db 
		$collection = $db->projects; // select collection 
		$pid = $_GET['pid']; // get the project Id.
		$project = $collection->findOne(array("_id"=>new MongoId($pid)));
		}
		catch (MongoException $e)
		{
			$e->getMessage();
		}
		$this->render('create_issue',array('project'=>$project,));

	}
	public function actionCreate()
	{
		try { 
		$mongo = new MongoClient(); // connect mongo 
		$db = $mongo->selectDB("mydb"); //select a db 
		$issue = $db->issues;
		$data['issue'] = $_POST['issue'];
		$data['desc'] = $_POST['desc'];
		$data['type'] = $_POST['type'];

		$data2['status'] = $_POST['status'];
		$data2['userid'] = $_POST['user'];
		$curdate = date("d/m/Y") ;
		$data2['create_date'] = new mongoDate(strtotime($curdate));
		$data['status'] = array();
		$data['status'][] = $data2;
		$issue->insert($data);
		}
		catch (MongoException $e)
		{
			$e->getMessage();
		}
		$this->actionIndex();
	}

	public function actionChangeIssue()
	{
		$id = $_GET['id'];
		try { 
		$mongo = new MongoClient();
		$db = $mongo->mydb;
		$c = $db->issues;
		$iss = $c->findOne(array("_id"=> new MongoId($id)));
		}
		catch(MongoException $e)
		{
			$e->getMessage();
		}
		$this->render('changeIssue',array('issue'=>$iss));

	}

	public function actionUpdateIssue()
	{
		try { 
		$mongo = new MongoClient();
		$db = $mongo->mydb;
		$c = $db->issues;
		$id = $_POST['id'];
		$data2['status'] = $_POST['status'];
		$data2['userid'] = $_POST['user'];
		$curdate = date("d/m/Y") ;
		$data2['create_date'] = new mongoDate(strtotime($curdate));
		$data['status'] = array();
		$data['status'] = $data2;
		$c->update(array("_id"=> new MongoId($id)),array('$push'=>$data));
		}
		catch (MongoException $e)
		{
			$e->getMessage();
		}
		$this->actionIndex();
	}
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}