<?php
 
class ProjectController extends Controller
{
	

	public function connectMongo($mydb,$c)
	{	
		try {
		$mongo = new MongoClient();
		$db = $mongo->selectDB($mydb);
		$collection = $db->c;
		return $collection;
		}
		catch(MongoException $e)
		{
			$e->getMessage();
		}
	}

	public function actionIndex()
	{	
		try {
		$mongo = new MongoClient(); // connect mongo 
		$db = $mongo->selectDB("mydb"); //select a db 
		$collection = $db->projects; // select collection 
		$p = $collection->find(); //list all the projects in the DB 
		}
		catch (MongoException $e)
		{
			$e->getMessage();
		}
		$this->render('index',array('projects' => $p));
	}

	
	public function actionRead()  // read operation is working. 
	{
		try 
		{
		$mongo = new MongoClient();
		$db = $mongo->selectDB("mydb");
		$collection = $db->projects;
		$mongo = new MongoClient();
		$db = $mongo->selectDB("mydb");
		$collection = $db->projects;
		//$prj = array('name'=>'name');
		$q = $_GET['name'];
		//echo $q;
		$query = array( "name" => $q); 
		$cursor = $collection->findOne($query);
		// get the list of team members. 
		$u = $db->users;
		
		//echo $cursor['members'][0]['userid'];
		$users = $u->find();
		}
		catch (MongoException $e)
		{
			$e->getMessage();
		}
		$this->render('read',array('project' => $cursor, 'users'=>$users));
	}

	public function actionDelete() // not yet done.
	{
		$this->render('delete');
	}

	public function actionNew()  //creates a new record 
	{
		//$collection = $this->connectMongo("mydb","users");
		try
		{ 
		$mongo = new MongoClient(); // connect mongo 
		$db = $mongo->selectDB("mydb"); //select a db 
		$collection = $db->users; // select collection 

		$cursor = $collection->find(); 
		}
		catch(MongoException $e)
		{
			$e->getMessage();
		}
		$this->render('create',array('users' => $cursor));
	}
	public function actionCreate() // writes new record to db. 
	{ 
		$data  = $_POST;
		try { 
		$mongo = new MongoClient();
		$db = $mongo->mydb;
		$collection = $db->projects;
		//$project = $data;
		$project['name'] = $data['name'];
		$project['desc'] = $data['desc'];
		$project['start_date'] = "1-6-2013";
		$project['status'] = 1;
		/*$i=0;
		/*$c = $db->users;
		$q = $data['uname1'];
		$query = array( "username" => $q); 
		$uname = $c->findOne($query);*/
		//echo "userid : ".$uname['userid'];
		//echo $data['role1']['role'];
		/*$project['team'][$i]['userid']=$data['uname1']; // the team is nested array. 
		$project['team'][$i]['role']= $data['role1'];
		$i++;
		$c = $db->users;
		$q = $data['uname2'];
		$query = array( "username" => $q);
		$uname = $c->findOne($query);
		$project['team'][$i]['userid']= $data['uname1'];
		$project['team'][$i]['role']= $data['role2'];*/

		$collection->insert($project);
		$p = $collection->find();
		}
		catch (MongoException $e)
		{
			 $e->getMessage();
		}

	 $this->render('index',array('projects'=> $p));	
	}

	public function actionNewMember()
	{	
		//$p = $this->connectMongo("mydb","projects");
		try { 
		$mongo = new MongoClient();
		$db = $mongo->mydb;
		$p = $db->projects;
		$projects = $p->find();
		$mongo = new MongoClient();
		$db = $mongo->mydb;
		$u = $db->users;
		//$u = connectMongo("mydb","users");
		$users = $u->find();
		}
		catch(MongoException $e)
		{
			$e->getMessage();
		}

		$this->render('new_member',array('projects'=>$projects,'users'=>$users));
	}
	public function actionRemoveUser()
	{
		try { 
		$mongo = new MongoClient();
		$db = $mongo->selectDB('mydb');
		$p = $db->projects;
		$pid = $_GET['pid'];
		$uid = $_GET['uid'];
		$p->update(array("_id"=> new MongoId($pid)),array('$pull'=>array('members'=>array("userid"=>$uid))));
		}
		catch(MongoException $e)
		{
			$e->getMessage();
		}
		$this->actionIndex();


	}

	public function actionAddMember()
	{	
		try { 
		$mongo = new MongoClient();
		$db = $mongo->mydb;
		$p = $db->projects;
		$project = $p->findOne(array("_id"=> new MongoId($_POST['project'])));
		//$p->update(array("_id"=> new MongoId($_POST['project'])),array('$push'=>array("userid"=>$_POST['user'],"role"=>$_POST['role'])));	
		/*foreach ($project['members'] as $u) {
			# code...
			if($u['userid'] == $_POST['user'])
		    {		
			echo "<h3> Cannot add user! this user is already added.</h3>";
			exit();
			}
		}*/
		$tm = $p->findOne(array("_id"=> new MongoId($_POST['project']),"members.userid"=>$_POST['user']));
		if (isset($tm))
		{
			// I want to flash a message here 
			echo "<h3> Cannot add user! this user is already added.</h3>";
			exit();
		}
		else
			$p->update(array("_id"=> new MongoId($_POST['project'])),array('$push'=>array('members'=>array("userid"=>$_POST['user'],"role"=>$_POST['role']))));
	}
		catch(MongoException $e)
		{
			$e->getMessage();
		}
		$this->actionIndex();
	}

	public function actionChangeRole()
	{
		try { 
		$mongo = new MongoClient();
		$db = $mongo->selectDB("mydb");
		$p= $db->projects;

		$q = $_GET['uid'];
		//echo $_GET['pid'];
		//$q=array(("_id"= new MongoId($_GET['pid'])));
		$project =$p->findOne(array("_id"=> new MongoId($_GET['pid'])));
		
		

		//$query = array( "userid" => $q); 
		//$r = $p->findOne("members"=>$query);*/
		//var_dump($_GET) ;
	 	//$pid = $CURRENT_PID;
		//$p->update(array("_id"=>new MongoId($_GET['uid'])),array('$set'));
		}

		catch (MongoException $e)
		{
			$e->getMessage();
		}
		$this->render('change_role',array('uid'=>$_GET['uid'],'project'=>$project));
			
	}
	public function actionUpdateRole()
	{	
		try { 
		$mongo = new MongoClient(); // connect mongo 
		$db = $mongo->selectDB("mydb"); //select a db 
		$p = $db->projects; // select collection 
		echo $_POST['role'];
		$project = $p->findOne(array("_id"=> new MongoId($_POST['pid'])));
		//	echo $project['_id'];
		$roleupdate = array("members.$.role"=>$_POST['role']);
		$p->update(array("_id"=>new MongoId($_POST['pid']),"members.userid"=>$_POST['uid']),array('$set'=>$roleupdate));
		}
		catch(MongoException $e)
		{
			$e->getMessage();
		}

		$this->actionIndex();
		
	}

	public function actionCreateIssue()
	{
		try { 
		$mongo = new MongoClient(); // connect mongo 
		$db = $mongo->selectDB("mydb"); //select a db 
		$collection = $db->users; // select collection 
		$u = $collection->find(); //list all users		
		}
		catch (MongoException $e )
		{
			$e->getMessage();
		}
		$pid = $_GET['pid']; // get the project Id and pass to view.
		$this->render('create_issue',array('pid'=>$pid,'user'=>$u));

	}

	

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
