<?php

?>

<div class="form">
<form action="<?php echo $this->createUrl('issue/create')?>" method="POST">
	
	<input type="text" name ="issue" placeholder = "Issue"><br />
	<textarea name = "desc" placeholder = "Description" cols=40 rows=6></textarea>  <br />
	<label>Type</label>
	<select name ="type">
		<option value = "Bug"> Bug</option>
		<option value = "Feature"> Feature </option>
		<option value = "Enhancement"> Enhancement </option>
	</select> <br />
	<label>Status</label>
	<select name="status">
		<option value ="Reported">Reported</option>
		<option value ="Started">Started</option>
		<option value ="Completed">Completed</option>
	</select> <br />
	<label>Users</label>
	<select name ="user">
	  <?php 
	  	$users = new User;
	  	foreach ($project['members'] as $user) {
	  			 $u = $users->getUser($user['userid']);
		  	echo "<option value=".$u['_id'].">".$u['username']."</option>"; // display user's list
		  } 
		  	
		  ?>
	</select>
	<br />
	
	 
	<button type="submit"> Create Issue </button> 
</form>

</div><!-- form -->