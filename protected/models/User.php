<?php

class User
{
	public function getUser($id)
	{
		try {
		$mongo = new MongoClient();
		$db = $mongo->selectDB('mydb');
		$collection = $db->users;
		$q = array('_id'=> new MongoId($id));
		$user = $collection->findOne($q);
		return $user;
		
		}
		catch(MongoException $e)
		{
			$e->getMessage();
		}
	}
}